const options = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Pricing Table Express API with Swagger",
      version: "0.0.1",
      description:
        "This is a simple CRUD API application made with Express and documented with Swagger",
      license: {
        name: "MIT",
        url: "https://spdx.org/licenses/MIT.html",
      },
      contact: {
        name: "DavidChiu",
        url: "https://gitlab.com/chingchiu169",
        email: "chingchiu169@gmail.com",
      },
    },
    servers: [
      {
        url: `http://localhost:${process.env.PORT}/`,
      },
    ],
  },
  apis: ["./routes/items.js", "./routes/plans.js"],
};

module.exports = options;
