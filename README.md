<p align="center">
  <a href="" rel="noopener">
 <img width=200px height=200px src="https://i.imgur.com/pUKopWQ.png" alt="Project logo"></a>
</p>

<h3 align="center">Pricing Service</h3>

---

## 📝 Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Deployment](#deployment)
- [Usage](#usage)
- [Built Using](#built_using)
- [Authors](#authors)
- [Acknowledgments](#acknowledgement)

## 🧐 About <a name = "about"></a>

A pricing table service with Restful API using NodeJs with Mysql

## 👀 Demo <a name = "demo"></a>

http://demo.davidchiu.work

## 🏁 Getting Started <a name = "getting_started"></a>

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [deployment](#deployment) for notes on how to deploy the project on a live system.

### 🧰 Prerequisites

```
- Node
- Docker
- Mysql
```

### ⚙️ Installing

```
1. git clone git@gitlab.com:demo169169/pricingtable/backend.git
2. cd backend
3. npm install
4. npm start
```

## 🔧 Running the tests <a name = "tests"></a>

```
- npm run test
```

## 🎈 Doc <a name="usage"></a>

- [API](/api-docs) - By Swagger

## 🚀 Deployment <a name = "deployment"></a>

```
1. docker build . -t pricing-backend
2. docker run -p 8000:8000 pricing-backend
```

## ⛏️ Built Using <a name = "built_using"></a>

- [NodeJs](https://nodejs.org/en/) - Server Environment
- [ExpressJs](https://expressjs.com/) - NodeJs Framework
- [Swagger](https://swagger.io/) - API Documentation

## ✍️ Authors <a name = "authors"></a>

- [@chingchiu169](https://gitlab.com/chingchiu169)

## 🎉 Acknowledgements <a name = "acknowledgement"></a>

```
- Enjoy coding
```