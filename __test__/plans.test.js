const app = require("../app");
const supertest = require("supertest");
const fetch = require("node-fetch");

let plan_id = "";
let items = [];
let price = 20;

beforeAll((done) => {
  supertest(app)
    .get("/items")
    .expect(200)
    .then((response) => {
      items = response.body.data.map((item) => item.item_id);
      done();
    });
});

afterAll((done) => {
  done();
});

test("POST /plans", async () => {
  await supertest(app)
    .post("/plans")
    .send({ name: "JEST", items: items, price: price })
    .expect(200)
    .then((response) => {
      expect(response.body.success === true).toBeTruthy();
      plan_id = response.body.data;
    });
});

test("GET /plans", async () => {
  await supertest(app)
    .get("/plans")
    .expect(200)
    .then((response) => {
      expect(Array.isArray(response.body.data)).toBeTruthy();
    });
});

test("PUT /plans", async () => {
  const item = items.pop();
  const newPrice = 30;
  await supertest(app)
    .put(`/plans/${plan_id}`)
    .send({ name: "JEST - PUT", items: [item], price: newPrice })
    .expect(200)
    .then((response) => {
      console.log(response.body)
      expect(response.body.success === true).toBeTruthy();
    });
});

test("DELETE /plans", async () => {
  await supertest(app)
    .delete(`/plans/${plan_id}`)
    .expect(200)
    .then((response) => {
      expect(response.body.success === true).toBeTruthy();
    });
});
