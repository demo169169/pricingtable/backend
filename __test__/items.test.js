const app = require("../app");
const supertest = require("supertest");

let item_id = "";

afterAll((done) => {
  done();
});

test("POST /items", async () => {
  await supertest(app)
    .post("/items")
    .send({ name: "JEST" })
    .expect(200)
    .then((response) => {
      expect(response.body.success === true).toBeTruthy();
      item_id = response.body.data;
    });
});

test("GET /items", async () => {
  await supertest(app)
    .get("/items")
    .expect(200)
    .then((response) => {
      expect(Array.isArray(response.body.data)).toBeTruthy();
    });
});

test("PUT /items", async () => {
  await supertest(app)
    .put(`/items/${item_id}`)
    .send({ name: "JEST - PUT" })
    .expect(200)
    .then((response) => {
      expect(response.body.success === true).toBeTruthy();
    });
});

test("DELETE /items", async () => {
  await supertest(app)
    .delete(`/items/${item_id}`)
    .expect(200)
    .then((response) => {
      expect(response.body.success === true).toBeTruthy();
    });
});
