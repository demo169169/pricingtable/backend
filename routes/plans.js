/**
 * @swagger
 * tags:
 *  name: Plans
 *  description: API to manage plans.
 */

/**
 * @swagger
 * components:
 *  schemas:
 *    Plan:
 *     properties:
 *        id:
 *          type: integer
 *        plan_id:
 *          type: string
 *        name:
 *          type: string
 *        items:
 *          type: array
 *          items:
 *            type: string
 *        price:
 *          type: integer
 *     required:
 *       - id
 *       - plan_id
 *       - name
 */

var express = require("express");
var router = express.Router();
var { uuid } = require("uuidv4");

const TABLE = "plans";
const RELATIONS = "relations";

/**
 * @swagger
 * paths:
 *  /plans:
 *    post:
 *      tags: [Plans]
 *      summary: Create a plan.
 *      requestBody:
 *       required: true
 *       content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              name:
 *                type: string
 *              items:
 *                type: array
 *                items:
 *                  type: string
 *              price:
 *                  type: integer
 *      responses:
 *        200:
 *          description: Success
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  success:
 *                    type: boolean
 *                  data:
 *                    type: string
 */
router.post("/", (req, res, next) => {
  var db = req.con;
  var { name, price, items } = req.body;
  var plan_id = uuid();
  db.beginTransaction((err) => {
    if (err) throw err;
    db.query(
      `INSERT INTO ${TABLE} (plan_id, name, price) VALUE ('${plan_id}', '${name}', ${price})`,
      (err2) => {
        if (err2) {
          return db.rollback(() => {
            res.json({ success: false, error: err2 });
          });
        }
        var sql = `INSERT INTO ${RELATIONS} (item_id, plan_id) VAlUE ?`;
        var values = items.map((item) => {
          return [item, plan_id];
        });
        db.query(sql, [values], (err3) => {
          if (err3) {
            return db.rollback(() => {
              res.json({ success: false, error: err3 });
            });
          }
          db.commit((err4) => {
            if (err4) {
              return db.rollback(() => {
                res.json({ success: false, error: err4 });
              });
            }
            res.json({ success: true, data: plan_id });
          });
        });
      }
    );
  });
});

/**
 * @swagger
 * paths:
 *  /plans:
 *    get:
 *      tags: [Plans]
 *      summary: Return a list of plans.
 *      responses:
 *        200:
 *          description: Success
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  success:
 *                    type: boolean
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Plan'
 */
router.get("/", (req, res, next) => {
  var db = req.con;
  db.query(
    `
    SELECT p.id, p.name, p.plan_id, p.price, GROUP_CONCAT(DISTINCT r.item_id) as items
    FROM ${TABLE} AS p
    LEFT JOIN ${RELATIONS} AS r ON p.plan_id = r.plan_id
    GROUP BY p.id
    `,
    (err, rows) => {
      if (err) {
        res.json({ success: false, data: [] });
        return;
      }
      var data = rows;
      var formattedData = data.map((item) => {
        return { ...item, items: item.items?.split(",") || [] };
      });
      res.json({
        success: true,
        data: formattedData,
      });
    }
  );
});

/**
 * @swagger
 * paths:
 *  /plans/{plan_id}:
 *    put:
 *      tags: [Plans]
 *      summary: Update data of a plan.
 *      parameters:
 *       - in: path
 *         name: plan_id
 *         required: true
 *         type: string
 *      requestBody:
 *       required: true
 *       content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              name:
 *                type: string
 *              items:
 *                type: array
 *                items:
 *                  type: string
 *              price:
 *                type: integer
 *      responses:
 *        200:
 *          description: Updated
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  success:
 *                    type: boolean
 *        500:
 *          description: Internal server error
 */
router.put("/:id", (req, res, next) => {
  var { name, price, items } = req.body;
  var plan_id = req.params.id;
  var db = req.con;
  db.beginTransaction((err) => {
    if (err) return res.json({ success: false, error: err });
    db.query(
      `UPDATE ${TABLE} SET name='${name}', price=${price} WHERE plan_id='${plan_id}'`,
      (err2, row2) => {
        if (err2) {
          return db.rollback(() => {
            res.json({ success: false, error: err2 });
          });
        }
        var itemsValues = items.map((item) => `'${item}'`).join(",");
        db.query(
          `DELETE FROM ${RELATIONS} WHERE plan_id='${plan_id}' AND item_id NOT IN (${itemsValues})`,
          (err3, row3) => {
            if (err3) {
              return db.rollback(() => {
                res.json({ success: false, error: err3 });
              });
            }
            // var sql = ;
            var values = items.map((item) => {
              return [item, plan_id];
            });
            db.query(
              `INSERT INTO ${RELATIONS} (item_id, plan_id) VALUES ? ON DUPLICATE KEY UPDATE item_id=VALUES(item_id), plan_id=VALUES(plan_id)`,
              [values],
              (err4, row4) => {
                if (err4) {
                  return db.rollback(() => {
                    res.json({ success: false, error: err4 });
                  });
                }
                db.commit((err5) => {
                  if (err5) {
                    return db.rollback(() => {
                      res.json({ success: false, error: err2 });
                    });
                  }
                  res.json({
                    success: true,
                  });
                });
              }
            );
          }
        );
      }
    );
  });
});

/**
 * @swagger
 * paths:
 *  /plans/{plan_id}:
 *    delete:
 *      tags: [Plans]
 *      summary: Delete a plan.
 *      parameters:
 *       - in: path
 *         name: plan_id
 *         required: true
 *         type: string
 *      responses:
 *        200:
 *          description: Deleted
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  success:
 *                    type: boolean
 *        500:
 *          description: Internal server error
 */
router.delete("/:id", (req, res, next) => {
  var plan_id = req.params.id;
  var db = req.con;
  db.query(
    `DELETE FROM ${TABLE}, ${RELATIONS} USING ${TABLE} INNER JOIN ${RELATIONS} WHERE ${TABLE}.plan_id = '${plan_id}' AND ${RELATIONS}.plan_id = ${TABLE}.plan_id`,
    (err) => {
      if (err) {
        res.json({ success: false, error: err });
        return;
      }
      res.json({
        success: true,
      });
    }
  );
});

module.exports = router;
