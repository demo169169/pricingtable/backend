/**
 * @swagger
 * tags:
 *  name: Items
 *  description: API to manage items.
 */

/**
 * @swagger
 * components:
 *  schemas:
 *    Item:
 *     properties:
 *        id:
 *          type: integer
 *        item_id:
 *          type: string
 *        name:
 *          type: string
 *     required:
 *       - id
 *       - item_id
 *       - name
 */

var express = require("express");
var router = express.Router();
var { uuid } = require("uuidv4");

const TABLE = "items";

/**
 * @swagger
 * paths:
 *  /items:
 *    post:
 *      tags: [Items]
 *      summary: Create a item.
 *      requestBody:
 *       required: true
 *       content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              name:
 *                type: string
 *      responses:
 *        200:
 *          description: Created
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  success:
 *                    type: boolean
 *                  data:
 *                    type: string
 *        500:
 *          description: Internal server error
 */
router.post("/", (req, res, next) => {
  var db = req.con;
  var name = req.body.name;
  var item_id = uuid();
  db.query(
    `INSERT INTO ${TABLE} (name, item_id) VALUE ('${name}', '${item_id}')`,
    (err, rows) => {
      if (err) {
        res.json({ success: false, error: err });
        return;
      }
      res.json({ success: true, data: item_id });
    }
  );
});

/**
 * @swagger
 * paths:
 *  /items:
 *    get:
 *      tags: [Items]
 *      summary: Return a list of items.
 *      responses:
 *        200:
 *          description: Success
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  success:
 *                    type: boolean
 *                  data:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Item'
 *        500:
 *          description: Internal server error
 */
router.get("/", (req, res, next) => {
  var db = req.con;
  db.query(`SELECT * FROM ${TABLE}`, (err, rows) => {
    if (err) {
      res.json({ success: false, data: [] });
      return;
    }
    var data = rows;
    res.json({ success: true, data: data });
  });
});

/**
 * @swagger
 * paths:
 *  /items/{item_id}:
 *    put:
 *      tags: [Items]
 *      summary: Update a item.
 *      parameters:
 *       - in: path
 *         name: item_id
 *         required: true
 *         type: string
 *      requestBody:
 *       required: true
 *       content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              name:
 *                type: string
 *      responses:
 *        200:
 *          description: Updated
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  success:
 *                    type: boolean
 *        500:
 *          description: Internal server error
 */
router.put("/:id", (req, res, next) => {
  var { name } = req.body;
  var item_id = req.params.id;
  var db = req.con;
  db.query(
    `UPDATE ${TABLE} SET name='${name}' WHERE item_id='${item_id}'`,
    (err, rows) => {
      if (err) {
        res.json({ success: false, error: err });
        return;
      }
      res.json({ success: true });
    }
  );
});

/**
 * @swagger
 * paths:
 *  /items/{item_id}:
 *    delete:
 *      tags: [Items]
 *      summary: Delete a item.
 *      parameters:
 *       - in: path
 *         name: item_id
 *         required: true
 *         type: string
 *      responses:
 *        200:
 *          description: Deleted
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  success:
 *                    type: boolean
 *        500:
 *          description: Internal server error
 */
router.delete("/:id", (req, res, next) => {
  var item_id = req.params.id;
  var db = req.con;
  db.query(`DELETE FROM ${TABLE} WHERE item_id = '${item_id}'`, (err) => {
    if (err) {
      res.json({ success: false, error: err });
      return;
    }
    res.json({
      success: true,
    });
  });
});

module.exports = router;
