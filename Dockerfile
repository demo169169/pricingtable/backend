FROM node:14-alpine

WORKDIR /usr/src/app

ENV NODE_ENV production
ENV PORT 8000

COPY package*.json ./
RUN yarn install
COPY . .

EXPOSE 8000
CMD [ "node", "./bin/www" ]

