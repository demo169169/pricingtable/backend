-- phpMyAdmin SQL Dump
-- version 4.9.7deb1
-- https://www.phpmyadmin.net/
--
-- 主機： localhost:3306
-- 產生時間： 2021 年 07 月 11 日 14:45
-- 伺服器版本： 8.0.25-0ubuntu0.21.04.1
-- PHP 版本： 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `pricing`
--

-- --------------------------------------------------------

--
-- 資料表結構 `items`
--

CREATE TABLE `items` (
  `id` int NOT NULL,
  `item_id` varchar(36) NOT NULL,
  `name` varchar(192) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- 傾印資料表的資料 `items`
--

INSERT INTO `items` (`id`, `item_id`, `name`) VALUES
(1, '714a70b6-67d1-48d2-9c5c-f6159c4db809', 'General'),
(2, 'e8cae78c-aff8-4871-91d9-fa39d46c517f', 'Specialist\r\n'),
(3, 'a4fd1f0b-1900-427c-a5db-2c549050b018', 'Physiotherapy');

-- --------------------------------------------------------

--
-- 資料表結構 `plans`
--

CREATE TABLE `plans` (
  `id` int NOT NULL,
  `plan_id` varchar(36) NOT NULL,
  `name` varchar(192) NOT NULL,
  `price` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- 傾印資料表的資料 `plans`
--

INSERT INTO `plans` (`id`, `plan_id`, `name`, `price`) VALUES
(1, '3495dcf4-11b3-4b0b-934b-986ef6dc163d', 'Standard', 0),
(2, '9b43807f-62d7-42f6-89f4-f8df5d6d89fe', 'Premium', 10);

-- --------------------------------------------------------

--
-- 資料表結構 `relations`
--

CREATE TABLE `relations` (
  `id` int NOT NULL,
  `item_id` varchar(36) NOT NULL,
  `plan_id` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- 傾印資料表的資料 `relations`
--

INSERT INTO `relations` (`id`, `item_id`, `plan_id`) VALUES
(1, 'a4fd1f0b-1900-427c-a5db-2c549050b018', '9b43807f-62d7-42f6-89f4-f8df5d6d89fe'),
(2, 'e8cae78c-aff8-4871-91d9-fa39d46c517f', '9b43807f-62d7-42f6-89f4-f8df5d6d89fe'),
(3, '714a70b6-67d1-48d2-9c5c-f6159c4db809', '9b43807f-62d7-42f6-89f4-f8df5d6d89fe'),
(4, '714a70b6-67d1-48d2-9c5c-f6159c4db809', '3495dcf4-11b3-4b0b-934b-986ef6dc163d');

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item_id` (`item_id`),
  ADD KEY `item_id_2` (`item_id`);

--
-- 資料表索引 `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plan_id` (`plan_id`),
  ADD KEY `plan_id_2` (`plan_id`);

--
-- 資料表索引 `relations`
--
ALTER TABLE `relations`
  ADD PRIMARY KEY (`id`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `items`
--
ALTER TABLE `items`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `plans`
--
ALTER TABLE `plans`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `relations`
--
ALTER TABLE `relations`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
